import json
import requests

octopus_server_uri = 'https://tirumala18.octopus.app'
octopus_api_key = 'API-NJDQ6NI5HVUFRD11PMSLNW2YQ7IOVTS'
headers = {'X-Octopus-ApiKey': octopus_api_key}


def get_octopus_resource(uri):
    response = requests.get(uri, headers=headers)
    response.raise_for_status()

    return json.loads(response.content.decode('utf-8'))


space_name = 'Default'
project_name = 'OctopusAutomation'
role_name = 'My role'
step_name="Deploy Amazon ECS Service"
spaces = get_octopus_resource('{0}/api/spaces/all'.format(octopus_server_uri))
space = next((x for x in spaces if x['Name'] == space_name), None)

projects = get_octopus_resource('{0}/api/{1}/projects/all'.format(octopus_server_uri, space['Id']))
project = next((x for x in projects if x['Name'] == project_name), None)

uri = '{0}/api/{1}/deploymentprocesses/{2}'.format(octopus_server_uri, space['Id'], project['DeploymentProcessId'])
process = get_octopus_resource(uri)

process['Steps'].remove('Deploy Amazon ECS Service')

response = requests.put(uri, headers=headers, json=process)
response.raise_for_status()
 

