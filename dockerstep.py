import json
import requests

octopus_server_uri = 'https://tirumala18.octopus.app'
octopus_api_key = 'API-NJDQ6NI5HVUFRD11PMSLNW2YQ7IOVTS'
headers = {'X-Octopus-ApiKey': octopus_api_key}


def get_octopus_resource(uri):
    response = requests.get(uri, headers=headers)
    response.raise_for_status()

    return json.loads(response.content.decode('utf-8'))


space_name = 'Default'
project_name = 'docker'
role_name = 'My role'

spaces = get_octopus_resource('{0}/api/spaces/all'.format(octopus_server_uri))
space = next((x for x in spaces if x['Name'] == space_name), None)

projects = get_octopus_resource('{0}/api/{1}/projects/all'.format(octopus_server_uri, space['Id']))
project = next((x for x in projects if x['Name'] == project_name), None)

uri = '{0}/api/{1}/deploymentprocesses/{2}'.format(octopus_server_uri, space['Id'], project['DeploymentProcessId'])
process = get_octopus_resource(uri)

process['Steps'].append( {
      #"Id": "b2c3d417-83f3-49bf-ab91-674be4d578aa",
      "Name": "Deploy Docker Stack from ECR",
      "PackageRequirement": "LetOctopusDecide",
      "Properties": {},
      "Condition": "Success",
      "StartTrigger": "StartAfterPrevious",
      "Actions": [
        {
       #   "Id": "98af07e6-609b-4dd7-af8e-53b8ca45d2d1",
          "Name": "Deploy Docker Stack from ECR",
          "ActionType": "Octopus.Script",
          "Notes": None,
          "IsDisabled": 'false',
          "CanBeUsedForProjectVersioning": 'false',
          "IsRequired": 'true',
          "WorkerPoolId": None,
          "Container": {
            "Image": None,
            "FeedId": None
          },
          "WorkerPoolVariable": "DOCKER_SWARM_POOL",
          "Environments": [],
          "ExcludedEnvironments": [],
          "Channels": [],
          "TenantTags": [],
          "Packages": [],
          "Condition": "Success",
          "Properties": {
            "Octopus.Action.Script.ScriptSource": "Inline",
            "Octopus.Action.Script.Syntax": "Bash",
            "Octopus.Action.Script.ScriptBody": "set -e\n\n# Install Latest kick-cli\nprintf \"Checking kick-cli ...\\n\"\nmajorVersion=$(get_octopusvariable \"docker-common-kick-version-major\")\nminorVersion=$(get_octopusvariable \"docker-common-kick-version-minor\")\npatchVersion=$(get_octopusvariable \"docker-common-kick-version-patch\")\nversion=$(kick --version 2> /dev/null || printf 'missing')\nif [ \"${version}\" == \"missing\" ]; then\n  installKick=\"yes\"\nelse\n  readarray -d . -t versionSplit <<< ${version/kick: /}\n  if [ ${versionSplit[0]} -lt ${majorVersion} ]; then\n    installKick=\"yes\"\n  else\n    if [ ${versionSplit[0]} -eq ${majorVersion} ] && [ ${versionSplit[1]} -lt ${minorVersion} ]; then\n      installKick=\"yes\"\n    else\n      if [ ${versionSplit[0]} -eq ${majorVersion} ] && [ ${versionSplit[1]} -eq ${minorVersion} ] && [ ${versionSplit[2]} -lt ${patchVersion} ]; then\n        installKick=\"yes\"\n      else\n        installKick=\"no\"\n      fi  \n    fi  \n  fi  \nfi;\n\nif [ \"${installKick}\" == \"yes\" ]; then\n  printf \"Installing kick-cli ...\\n\"\n  sudo npm install -g kick-cli --registry=https://npmjs.libera.com\nfi\n\n# Set variables\nregistry=$(get_octopusvariable \"DEPLOY_STACK_TEMPLATE_REGISTRY\");\n\n# Login into Docker Registry\naws ecr get-login-password --region us-gov-west-1 | docker login --username AWS --password-stdin 387565646748.dkr.ecr.us-gov-west-1.amazonaws.com\n#if [ \"${registry}\" == \"docker-registry.libera.com\" ]; then\n#  get_octopusvariable \"REGISTRY_PASSWORD\" | \\\n #   docker login \\\n #     --username=\"libera\" \\\n #     --password-stdin \\\n#      \"https://docker-registry.libera.com\" 2>&1\n#fi\n\n# Determine Debug \ndebug=$(get_octopusvariable \"docker-common-debug\");\n\n# Save Variables JSON\necho $(get_octopusvariable \"Octopus.Action[Build Variables JSON].Output.variables_json\") > vars.json\n\n# If debug, then Upload Variables JSON\nif [ \"$debug\" == \"True\" ]; then\n\tnew_octopusartifact \"vars.json\" \"variables.json\"\nfi\n\n# Determine which flags to activate\nremoveStack=$(get_octopusvariable \"docker-common-remove-stack\");\nremoveStackAllowFailure=\"True\";\nloadSwarmVars=\"False\";\n\nflagDebug=\"\";\nif [ \"$debug\" == \"True\" ]; then\n\tflagDebug='--debug';\nfi\n\nflagRemove=\"\";\nif [ \"$removeStack\" == \"True\" ]; then\n\tflagRemove='--remove';\nfi\n\nflagRemoveAllowFail=\"\";\nif [ \"$removeStackAllowFailure\" == \"True\" ]; then\n\tflagRemoveAllowFail='--remove-allow-fail';\nfi\n\nflagLoadSwarmVars=\"\";\nif [ \"$loadSwarmVars\" == \"True\" ]; then\n\tflagLoadSwarmVars='--load-swarm-vars';\nfi\n\n# Run kick to deploy system\nkick system deploy \\\n--name=$(get_octopusvariable \"DEPLOY_STACK_TEMPLATE_STACK_NAME\") \\\n--type=$(get_octopusvariable \"DEPLOY_STACK_TEMPLATE_STACK_TYPE\") \\\n--zone=$(get_octopusvariable \"DEPLOY_STACK_REXRAY_ZONE\") \\\n--file=\"empty\" \\\n--registry=\"${registry}\" \\\n--var-file=vars.json \\\n--wait=$(get_octopusvariable \"deploy-wait-time\") \\\n$flagDebug $flagRemove $flagRemoveAllowFail $flagLoadSwarmVars\n\n\n# Delete Variables JSON\nrm -f vars.json\n",
            "Octopus.Action.Template.Version": "3",
            "Octopus.Action.Template.Id": "ActionTemplates-718",
            "DEPLOY_STACK_TEMPLATE_STACK_TYPE": "worker",
            "DEPLOY_STACK_TEMPLATE_REGISTRY": "387565646748.dkr.ecr.us-gov-west-1.amazonaws.com",
            "Octopus.Action.RunOnServer": "true",
            "DEPLOY_STACK_TEMPLATE_STACK_NAME": "#{STACK_NAME}"
          },
          "Links": {}
        }
      ]
    }
  )

response = requests.put(uri, headers=headers, json=process)
response.raise_for_status()