import json
import requests
import os

octopus_server_uri="https://tirumala18.octopus.app/"
octopus_api_key = os.environ.get('apikey')
headers = {'X-Octopus-ApiKey': octopus_api_key}
step_name="Deploy Amazon ECS Service"
project_name="OctopusAutomation"
space_name="Default"

"""
subnetlist=os.environ.get('subnetlist')
securitygroups=os.environ.get('securitygroups')
ECSManagedTags=os.environ.get('ECSManagedTags')
def convert_dict(tolist):
  clist=tolist.split(", ")
  temp=[]
  for i in clist:
    tempdict = dict()
    tempdict['id'] = i
    temp.append(tempdict)
  return temp  
"""





def get_octopus_resource(uri):
    response = requests.get(uri, headers=headers)
    response.raise_for_status()

    return json.loads(response.content.decode('utf-8'))




spaces = get_octopus_resource('{0}/api/spaces/all'.format(octopus_server_uri))
space = next((x for x in spaces if x['Name'] == space_name), None)

def get_by_name(uri, name):
    resources = get_octopus_resource(uri)
    return next((x for x in resources if x['Name'] == name), None)
    

project = get_by_name('{0}/api/{1}/projects/all'.format(octopus_server_uri, space['Id']), project_name)

print("output",project)



#projects = get_octopus_resource('{0}/api/{1}/projects/all'.format(octopus_server_uri, space['Id']))
#project =  next((x for x in projects if x['Name'] == project_name), None)

uri = '{0}/api/{1}/deploymentprocesses/{2}'.format(octopus_server_uri, space['Id'], project['DeploymentProcessId'])
process = get_octopus_resource(uri)

print(process)

if step_name in process['Steps']:
  print("yes")
else:
  print("no")

"""
process['Steps'].append({
      "Name": "Deploy Amazon ECS Service",
      "PackageRequirement": "AfterPackageAcquisition",
      "Properties": {
        "Octopus.Action.TargetRoles": os.environ.get('role')
      },
      "Condition": "Success",
      "StartTrigger": "StartAfterPrevious",
      "Actions": [
        {
          "Name": "Deploy Amazon ECS Service",
          "ActionType": "aws-ecs",
          "Notes": None,
          "IsDisabled": 'false',
          "CanBeUsedForProjectVersioning": 'true',
          "IsRequired": 'false',
          "WorkerPoolId": None,
          "Container": {
            "Image": None,
            "FeedId": None
          },
          "WorkerPoolVariable": "",
          "Environments": [],
          "ExcludedEnvironments": [],
          "Channels": [],
          "TenantTags": [],
          "Packages": [
            {
             
              "Name": os.environ.get('packagename'), 
              "PackageId":os.environ.get('packagename'),
              "FeedId":os.environ.get('FeedId') #"Feeds-1012",
              "AcquisitionLocation": "NotAcquired",
              
              "Properties": {}
            }
          ],
          "Condition": "Success",
          "Properties": {},
          "StepPackageVersion": "1.1.1",
          "LastSavedStepPackageVersion": "1.1.1",
          "Inputs": {
            "name":os.environ.get('taskdefname') #"colortestdef",
            "containers": [
              {
                "containerName": os.environ.get('containername'),
                "containerImageReference": {

                  "imageName": os.environ.get('imagename'),
                  "feedId":os.environ.get('FeedId') #"Feeds-1012"
                },
                "repositoryAuthentication": {
                  "type": "default"
                },
                "memoryLimitSoft": os.environ.get('memoryLimitSoft'),
                "memoryLimitHard": os.environ.get('memoryLimitHard'),
                "containerPortMappings": [
                  {
                    "containerPort": os.environ.get('containerPort'),
                    "protocol":os.environ.get('protocol')# "tcp"
                  }
                ],
                "essential": 'true',
                "environmentFiles": [],
                "environmentVariables": [],
                "networkSettings": {
                  "disableNetworking": 'false',
                  "dnsServers": [],
                  "dnsSearchDomains": [],
                  "extraHosts": []
                },
                "containerStorage": {
                  "readOnlyRootFileSystem": 'false',
                  "mountPoints": [],
                  "volumeFrom": []
                },
                "containerLogging": {
                  "type": "auto"
                },
                "dockerLabels": [],
                "healthCheck": {
                  "command": [],
                  "interval":os.environ.get('healthCheckinterval') 30,
                  "timeout":os.environ.get('healthChecktimeout') 5
                },
                "dependencies": [],
                "stopTimeout":os.environ.get('stopTimeout') 2,
                "ulimits": []
              }
            ],
            "task": {
              "taskExecutionRole":os.environ.get('taskExecutionRole') #"arn:aws:iam::841777056658:role/ecstaskexecutionrole",
              "cpu":os.environ.get('cpu') #1024,
              "memory": os.environ.get('taskmemory')#2048,
              "runtimePlatform": {
                "cpuArchitecture": os.environ.get('cpuArchitecture')#"X86_64",
                "operatingSystemFamily":os.environ.get('operatingSystemFamily') #"LINUX"
              },
              "volumes": []
            },
            "networkConfiguration": {
              "securityGroupIds":convert_dict(securitygroups),
              "subnetIds": convert_dict(subnetlist),
              "autoAssignPublicIp": os.environ.get('autoAssignPublicIp')#'true'
            },
            "desiredCount": os.environ.get('desiredCount'),
            "additionalTags": {
              "enableEcsManagedTags": 'true',
              "tags": convert_dict(ECSManagedTags)
            },
            "minimumHealthPercent":os.environ.get('minimumHealthPercent') #100,
            "maximumHealthPercent":os.environ.get('maximumHealthPercent') #200,
            "waitOption": {
              "type": os.environ.get('waitOptiontype')#"waitUntilCompleted"
            },
            "loadBalancerMappings": [
              {
                "containerName":os.environ.get('containerPort')  # "colortest",
                "containerPort": os.environ.get('containerPort')#80,
                "targetGroupArn": os.environ.get('targetGroupArn')# "arn:aws:elasticloadbalancing:us-east-1:841777056658:targetgroup/octo/c204b6e203b43948"
              }
            ]
          },
          "AvailableStepPackageVersions": [
            "1.1.1"
          ],
          "Links": {}
        }
      ]
    })
"""
response = requests.put(uri, headers=headers, json=process)
response.raise_for_status()